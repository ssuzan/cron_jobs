#setting date to run
if [ -z "$1" ]
  then
    echo "No argument supplied- please run: sh tar_and_archive.sh [dd_mm_yyyy or auto]"
    exit 1
fi

if [ $1 = "auto" ];
  then
    date="$(date -d "-1 days" +'%d_%m_%Y')"
else
    date="$1"
fi

# checking if already back up
file="$WORKSPACE/DB_backups/archive/.last_copied.txt"
if [ ! -f "$file" ]
  then
    echo "$0: File '${file}' not found."
  else
    while IFS= read -r line
    do
        # display $line or do somthing with $line
        #echo "$line"
        #echo "$date"
        if [ $line = $date ];
          then
            printf '%s has already been archived, exiting\n' "$line"
            exit 1
        fi
    done <"$file"
fi

echo "starting backups for date" $date
echo $WORKSPACE/DB_backups/





#data_from_geotab
echo "working on Geotab:"
find $WORKSPACE/DB_backups/data_from_geotab/database_original | grep $date  > /tmp/test.manifest
temp=$(cat /tmp/test.manifest | wc -l)
if [ $temp -gt 0 ]
  then
     if tar -cvzf /home/ubuntu/workspaces/DB_backups/archive/data_from_geotab/old_geotab_original_live_lv_$date.tar.gz  --files-from /tmp/test.manifest
     #  #archiving to S3:
       then
         echo "archving to S3- geotab:"
         if s3cmd put $WORKSPACE/DB_backups/archive/data_from_geotab s3://waycare-datastorage-1/archive/ --recursive
          then
           placeholder=1
          else
          echo "Geotab S3 archive failed"
        fi 
    
          #  #archiving to glacier:
         echo "archving to clacier- geotab:"
         if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_geotab/old_geotab_original_live_lv_$date.tar.gz -d "geotab_original_$date"   
          then
           placeholder=1
          else
          echo "Geotab Glacier archive failed"
        fi 
           #cleaning up:
         find $WORKSPACE/DB_backups/data_from_geotab/database_original | grep $date | xargs rm -v
         rm $WORKSPACE/DB_backups/archive/data_from_geotab/old_geotab_original_live_lv_$date.tar.gz
       else
         echo "Geotab archive failed"
     fi
fi

#tar -cvzf $WORKSPACE/DB_backups/archive/data_from_geotab/geotab_live_lv_$date.tar.gz  $WORKSPACE/DB_backups/data_from_geotab/database_original/geotab_fsp_location_live_lv_$date*.txt
##rm $WORKSPACE/DB_backups/data_from_geotab/database_original/geotab_fsp_location_live_lv_$date*.txt
#

##data_from_wu
#echo "working on WU:"
#find $WORKSPACE/DB_backups/data_from_wu/database_original | grep $date  > /tmp/test.manifest
#temp=$(cat /tmp/test.manifest | wc -l)
#if [ $temp -gt 0 ]
#  then
#     if tar -cvzf /home/ubuntu/workspaces/DB_backups/archive/data_from_wu/weather_underground_original_live_lv_$date.tar.gz  --files-from /tmp/test.manifest
#       then
#        #  #archiving to S3:
#        echo "archving to S3- WU:"
#        if s3cmd put $WORKSPACE/DB_backups/archive/data_from_wu s3://waycare-datastorage-1/archive/ --recursive
#          then
#           placeholder=1
#          else
#          echo "WU S3 archive failed"
#        fi 
#   
#         #  #archiving to glacier:
#        echo "archving to clacier- WU:"
#         if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_wu/weather_underground_original_live_lv_$date.tar.gz -d "wu_original_$date"
#          then
#           placeholder=1
#          else
#          echo "WU glacier archive failed"
#        fi 
#  
#          #cleaning up:
#        find $WORKSPACE/DB_backups/data_from_wu/database_original | grep $date | xargs rm -v
#        rm $WORKSPACE/DB_backups/archive/data_from_wu/weather_underground_original_live_lv_$date.tar.gz
#       else
#         echo "Weather Underground archive failed"
#     fi
#fi
#
#
###  data_from_wu  
##tar -cvzf $WORKSPACE/DB_backups/archive/data_from_wu/weather_underground_live_lv_$date.tar.gz $WORKSPACE/DB_backups/data_from_wu/database_original/KLAS_$date*.json
###rm $WORKSPACE/DB_backups/data_from_wu/database_original/KLAS_$date*.json
#
##data_from_wu_forecast
#echo "working on WU Forecast:"
#find $WORKSPACE/DB_backups/data_from_wu_forecast/database_original | grep $date  > /tmp/test.manifest
#temp=$(cat /tmp/test.manifest | wc -l)
#if [ $temp -gt 0 ]
#  then
#     if tar -cvzf /home/ubuntu/workspaces/DB_backups/archive/data_from_wu_forecast/wu_forecast_original_$date.tar.gz  --files-from /tmp/test.manifest
#       then
#        #  #archiving to S3:
#        echo "archiving to S3- WU Forecast:"
#        if s3cmd put $WORKSPACE/DB_backups/archive/data_from_wu_forecast s3://waycare-datastorage-1/archive/ --recursive
#          then
#           placeholder=1
#          else
#          echo "WU Forecast S3 archive failed"
#        fi 
#
#   
#         #  #archiving to glacier:
#       # echo "archiving to clacier- WU Forecast:"
#       # if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_wu_forecast/wu_forecast_original_$date.tar.gz -d "wu_forecast_original_$date"
#       #  then #success
#       #    placeholder=1
#       #  else
#       #   echo "WU Forecast glacier archive failed"
#       # fi 
#       echo "WU Forecast data is too small for  glacier archive script"
#
#  
#          #cleaning up:
#        find $WORKSPACE/DB_backups/data_from_wu_forecast/database_original | grep $date | xargs rm -v
#        rm $WORKSPACE/DB_backups/archive/data_from_wu_forecast/wu_forecast_original_live_lv_$date.tar.gz
#       else
#         echo "Weather Underground Forecast archive failed"
#     fi
#fi



##  data_from_wu_forecast  
#tar -cvzf $WORKSPACE/DB_backups/archive/data_from_wu_forecast/wu_forecast_$date.tar.gz $WORKSPACE/DB_backups/data_from_wu_forecast/database_original/*_$date*.json
##rm $WORKSPACE/DB_backups/data_from_wu/database_original/KLAS_$date*.json
#

##  data_from_fast_dms 
echo "working on FAST DMS:"
find $WORKSPACE/DB_backups/data_from_fast_dms/database_original | grep $date  > /tmp/test.manifest
temp=$(cat /tmp/test.manifest | wc -l)
if [ $temp -gt 0 ]
  then
     if tar -cvzf /home/ubuntu/workspaces/DB_backups/archive/data_from_fast_dms/old_fast_dms_original_$date.tar.gz  --files-from /tmp/test.manifest
       then
        #  #archiving to S3:
        echo "archiving to S3- FAST DMS:"
        if s3cmd put $WORKSPACE/DB_backups/archive/data_from_fast_dms s3://waycare-datastorage-1/archive/ --recursive 
          then #success
           placeholder=1
          else
           echo "FAST DMS S3 archive failed"
        fi 

         #  #archiving to glacier:
        echo "archiving to clacier- FAST DMS:"
        if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_fast_dms/old_fast_dms_original_$date.tar.gz -d "fast_dms_original_$date"
          then #success
           placeholder=1
          else
           echo "FAST DMS Glacier archive failed"
        fi 

  
          #cleaning up:
        find $WORKSPACE/DB_backups/data_from_fast_dms/database_original | grep $date | xargs rm -v
        rm $WORKSPACE/DB_backups/archive/data_from_fast_dms/old_fast_dms_original_$date.tar.gz
       else
         echo "FAST DMS archive failed"
     fi
fi


##data_from_fast_dms   
#tar -cvzf $WORKSPACE/DB_backups/archive/data_from_fast_dms/data_from_fast_dms_live_lv_$date.tar.gz $WORKSPACE/DB_backups/data_from_fast_dms/database_original/data_from_dms_$date*.xml
##rm $WORKSPACE/DB_backups/data_from_fast_dms/database_original/data_from_dms_$date*.xml
#

##data_from_wz
#echo "working on WZ:"
#find $WORKSPACE/DB_backups/data_from_wz/database_original | grep $date  > /tmp/test.manifest
#temp=$(cat /tmp/test.manifest | wc -l)
#if [ $temp -gt 0 ]
#  then
#     if tar -cvzf /home/ubuntu/workspaces/DB_backups/archive/data_from_wz/waze_traffic_alerts_original_lv_$date.tar.gz  --files-from /tmp/test.manifest
#       then
#        #  #archiving to S3:
#        echo "archiving to S3- WZ:"
#        if s3cmd put $WORKSPACE/DB_backups/archive/data_from_wz s3://waycare-datastorage-1/archive/ --recursive
#          then
#           placeholder=1
#          else
#          echo "WZ S3 archive failed"
#        fi 
#
#   
#         #  #archiving to glacier:
#        echo "archiving to clacier- WZ:"
#        if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_wz/waze_traffic_alerts_original_lv_$date.tar.gz -d "wz_original_$date"
#         then #success
#           placeholder=1
#         else
#          echo "WZ glacier archive failed"
#        fi 
#  
#          #cleaning up:
#        find $WORKSPACE/DB_backups/data_from_wz/database_original | grep $date | xargs rm -v
#        rm $WORKSPACE/DB_backups/archive/data_from_wz/waze_traffic_alerts_original_lv_$date.tar.gz
#       else
#         echo "WZ archive failed"
#     fi
#fi


##    data_from_wz  
#tar -cvzf $WORKSPACE/DB_backups/archive/data_from_wz/waze_traffic_alerts_lv_$date.tar.gz $WORKSPACE/DB_backups/data_from_wz/database_original/waze_traffic_alerts_lv_$date*.json
##rm $WORKSPACE/DB_backups/data_from_wz/database_original/vwaze_traffic_alerts_lv_$date*.json
#
##    data_from_wz_jam
echo "working on WZ JAM:"
find $WORKSPACE/DB_backups/data_from_wz_jam/database_original | grep $date  > /tmp/test.manifest
temp=$(cat /tmp/test.manifest | wc -l)
if [ $temp -gt 0 ]
  then
     if tar -cvzf /home/ubuntu/workspaces/DB_backups/archive/data_from_wz_jam/waze_traffic_jam_original_lv_$date.tar.gz  --files-from /tmp/test.manifest
       then
        #  #archiving to S3:
        echo "archiving to S3- WZ JAM:"
        if s3cmd put $WORKSPACE/DB_backups/archive/data_from_wz_jam s3://waycare-datastorage-1/archive/ --recursive
          then
           placeholder=1
          else
          echo "WZ S3 archive failed"
        fi 

   
         #  #archiving to glacier:
        echo "archiving to clacier- WZ JAM:"
        if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_wz_jam/waze_traffic_jam_original_lv_$date.tar.gz -d "wz_jam_original_$date"
         then #success
           placeholder=1
         else
          echo "WZ JAM glacier archive failed"
        fi 
  
          #cleaning up:
        find $WORKSPACE/DB_backups/data_from_wz_jam/database_original | grep $date | xargs rm -v
        rm $WORKSPACE/DB_backups/archive/data_from_wz_jam/waze_traffic_jam_original_lv_$date.tar.gz
       else
         echo "WZ JAM archive failed"
     fi
fi


##         data_from_wz_jam
#tar -cvzf $WORKSPACE/DB_backups/archive/data_from_wz_jam/waze_traffic_jam_lv_$date.tar.gz $WORKSPACE/DB_backups/data_from_wz_jam/database_original/waze_traffic_jam_lv_$date*.json
##rm $WORKSPACE/DB_backups/data_from_wz_jam/database_original/waze_traffic_jam_lv_$date*.json
#
#
#  data_from_wz_irregularities
echo "working on WZ IRREGULARITIES:"
find $WORKSPACE/DB_backups/data_from_wz_irregularities/database_original | grep $date  > /tmp/test.manifest
temp=$(cat /tmp/test.manifest | wc -l)
if [ $temp -gt 0 ]
  then
     if tar -cvzf /home/ubuntu/workspaces/DB_backups/archive/data_from_wz_irregularities/waze_traffic_irregularities_original_lv_$date.tar.gz  --files-from /tmp/test.manifest
       then
        #  #archiving to S3:
        echo "archiving to S3- WZ IRREGULARITIES:"
        if s3cmd put $WORKSPACE/DB_backups/archive/data_from_wz_irregularities s3://waycare-datastorage-1/archive/ --recursive
          then
           placeholder=1
          else
          echo "WZ S3 archive failed"
        fi 

   
         #  #archiving to glacier:
        echo "archiving to clacier- WZ IRREGULARITIES:"
        if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_wz_irregularities/waze_traffic_irregularities_original_lv_$date.tar.gz -d "wz_irregularities_original_$date"
         then #success
           placeholder=1
         else
          echo "WZ IRREGULARITIES glacier archive failed"
        fi 
  
          #cleaning up:
        find $WORKSPACE/DB_backups/data_from_wz_irregularities/database_original | grep $date | xargs rm -v
        rm $WORKSPACE/DB_backups/archive/data_from_wz_irregularities/waze_traffic_irregularities_original_lv_$date.tar.gz
       else
         echo "WZ IRREGULARITIES archive failed"
     fi
fi



##data_from_wz_irregularities
#tar -cvzf $WORKSPACE/DB_backups/archive/data_from_wz_irregularities/waze_traffic_irregularities_lv_$date.tar.gz $WORKSPACE/DB_backups/data_from_wz_irregularities/database_original/waze_traffic_irregularities_lv_$date*.json
##rm $WORKSPACE/DB_backups/data_from_wz_irregularities/database_original/waze_traffic_irregularities_lv_$date*.json
#
#moved to machine .221
##  data_from_fast_sensors
#echo "working on FAST SENSORS:"
#find $WORKSPACE/DB_backups/data_from_fast_sensors/database_original | grep $date  > /tmp/test.manifest
#temp=$(cat /tmp/test.manifest | wc -l)
#if [ $temp -gt 0 ]
#  then
#     if tar -cvzf /home/ubuntu/workspaces/DB_backups/archive/data_from_fast_sensors/sensors_data_original_lv_$date.tar.gz  --files-from /tmp/test.manifest
#       then
#        #  #archiving to S3:
#        echo "archiving to S3- SENSORS:"
#        if s3cmd put $WORKSPACE/DB_backups/archive/data_from_fast_sensors s3://waycare-datastorage-1/archive/ --recursive
#          then
#           placeholder=1
#          else
#          echo "SENSORS S3 archive failed"
#        fi 
#
#   
#         #  #archiving to glacier:
#        echo "archiving to clacier- SENSORS:"
#        if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_fast_sensors/sensors_data_original_lv_$date.tar.gz -d "fast_sensors_original_$date"
#         then #success
#           placeholder=1
#         else
#          echo "SENSORS glacier archive failed"
#        fi 
#  
#          #cleaning up:
#        find $WORKSPACE/DB_backups/data_from_fast_sensors/database_original | grep $date | xargs rm -v
#        rm $WORKSPACE/DB_backups/archive/data_from_fast_sensors/sensors_data_original_lv_$date.tar.gz
#       else
#         echo "SENSORS archive failed"
#     fi
#fi


#  data_from_gr
echo "working on GR:"
find $WORKSPACE/DB_backups/data_from_gr/database_original  > /tmp/test.manifest
temp=$(cat /tmp/test.manifest | wc -l)
if [ $temp -gt 0 ]
  then
     if tar -cvzf /home/ubuntu/workspaces/DB_backups/archive/data_from_gr/gr_data_original_lv_$date.tar.gz  --files-from /tmp/test.manifest
       then
        #  #archiving to S3:
        echo "archiving to S3- GR:"
        if s3cmd put $WORKSPACE/DB_backups/archive/data_from_gr s3://waycare-datastorage-1/archive/ --recursive
          then
           placeholder=1
          else
          echo "GR S3 archive failed"
        fi 

   
         #  #archiving to glacier:
        echo "archiving to clacier- GR:"
        if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_gr/gr_data_original_lv_$date.tar.gz -d "gr_data_original_$date"
         then #success
           placeholder=1
         else
          echo "GR glacier archive failed"
        fi 
  
          #cleaning up:
        #find $WORKSPACE/DB_backups/data_from_gr/database_original | xargs rm -v
        rm $WORKSPACE/DB_backups/archive/data_from_gr/gr_data_original_lv_$date.tar.gz
       else
         echo "GR archive failed"
     fi
fi

##  data_from_ndot
#echo "working on NDOT 511:"
#find $WORKSPACE/DB_backups/data_from_ndot/database_original  > /tmp/test.manifest
#temp=$(cat /tmp/test.manifest | wc -l)
#if [ $temp -gt 0 ]
#  then
#     if tar -cvzf /home/ubuntu/workspaces/DB_backups/archive/data_from_ndot/ndot_data_original_lv_$date.tar.gz  --files-from /tmp/test.manifest
#       then
#        #  #archiving to S3:
#        echo "archiving to S3- NDOT 511:"
#        if s3cmd put $WORKSPACE/DB_backups/archive/data_from_ndot s3://waycare-datastorage-1/archive/ --recursive
#          then
#           placeholder=1
#          else
#          echo "NDOT 511 S3 archive failed"
#        fi 
#
#   
#         #  #archiving to glacier:
#        echo "archiving to clacier- NDOT 511:"
#        if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_ndot/ndot_data_original_lv_$date.tar.gz -d "ndot_data_original_$date"
#         then #success
#           placeholder=1
#         else
#          echo "NDOT 511 glacier archive failed"
#        fi 
#  
#          #cleaning up:
#        find $WORKSPACE/DB_backups/data_from_ndot/database_original | xargs rm -v
#        rm $WORKSPACE/DB_backups/archive/data_from_ndot/ndot_data_original_lv_$date.tar.gz
#       else
#         echo "NDOT 511 archive failed"
#     fi
#fi
#
#echo "working on NDOT SP:"
#find $WORKSPACE/DB_backups/data_from_ndot_sp/database_original  > /tmp/test.manifest
#temp=$(cat /tmp/test.manifest | wc -l)
#if [ $temp -gt 0 ]
#  then
#     if tar -cvzf /home/ubuntu/workspaces/DB_backups/archive/data_from_ndot_sp/ndot_sp_data_original_lv_$date.tar.gz  --files-from /tmp/test.manifest
#       then
#        #  #archiving to S3:
#        echo "archiving to S3- NDOT SP:"
#        if s3cmd put $WORKSPACE/DB_backups/archive/data_from_ndot_sp s3://waycare-datastorage-1/archive/ --recursive
#          then
#           placeholder=1
#          else
#          echo "NDOT SP S3 archive failed"
#        fi 
#
#   
#         #  #archiving to glacier:
#        echo "archiving to clacier- NDOT SP:"
#        if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_ndot_sp/ndot_sp_data_original_lv_$date.tar.gz -d "ndot_sp_data_original_$date"
#         then #success
#           placeholder=1
#         else
#          echo "NDOT SP glacier archive failed"
#        fi 
#  
#          #cleaning up:
#        find $WORKSPACE/DB_backups/data_from_ndot_sp/database_original | xargs rm -v
#        rm $WORKSPACE/DB_backups/archive/data_from_ndot_sp/ndot_sp_data_original_lv_$date.tar.gz
#       else
#         echo "NDOT SP archive failed"
#     fi
#fi


echo "working on Traffic Lights LV:"
find $WORKSPACE/DB_backups/data_from_traffic_lights_lv/database_original  > /tmp/test.manifest
temp=$(cat /tmp/test.manifest | wc -l)
if [ $temp -gt 0 ]
  then
     if tar -cvzf /home/ubuntu/workspaces/DB_backups/archive/data_from_traffic_lights_lv/traffic_lights_lv_original_lv_$date.tar.gz  --files-from /tmp/test.manifest
       then
        #  #archiving to S3:
        echo "archiving to S3- Traffic Lights LV:"
        if s3cmd put $WORKSPACE/DB_backups/archive/data_from_traffic_lights_lv s3://waycare-datastorage-1/archive/ --recursive
          then
           placeholder=1
          else
          echo "Traffic Lights LV S3 archive failed"
        fi 

   
         #  #archiving to glacier:
        echo "archiving to clacier- Traffc Lights LV:"
        if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_traffic_lights_lv/traffic_lights_lv_original_lv_$date.tar.gz -d "traffic_lights_lv_data_original_$date"
         then #success
           placeholder=1
         else
          echo "Traffic Lights LV glacier archive failed"
        fi 
  
          #cleaning up:
        find $WORKSPACE/DB_backups/data_from_traffic_lights_lv/database_original | xargs rm -v
        rm $WORKSPACE/DB_backups/archive/data_from_traffic_lights_lv/traffic_lights_lv_original_lv_$date.tar.gz
       else
         echo "Traffic Lights LV archive failed"
     fi
fi





#
######################################
##archiving in s3:
#s3cmd put $WORKSPACE/DB_backups/archive s3://waycare-datastorage-1/ --recursive
#
##removing local copies
##rm $WORKSPACE/DB_backups/archive/data_from_geotab/geotab_live_lv_$date.tar.gz
##rm $WORKSPACE/DB_backups/archive/data_from_wu/weather_underground_live_lv_$date.tar.gz
##rm $WORKSPACE/DB_backups/archive/data_from_wu_forecast/wu_forecast_$date.tar.gz
##rm $WORKSPACE/DB_backups/archive/data_from_fast_dms/data_from_fast_dms_live_lv_$date.tar.gz
##rm $WORKSPACE/DB_backups/archive/data_from_wz/waze_traffic_alerts_lv_$date.tar.gz
##rm $WORKSPACE/DB_backups/archive/data_from_wz_jam/waze_traffic_jam_lv_$date.tar.gz
##rm $WORKSPACE/DB_backups/archive/data_from_wz_irregularities/waze_traffic_irregularities_lv_$date.tar.gz
##rm $WORKSPACE/DB_backups/archive/data_from_fast_sensors/sensor_data_$date.tar.gz
##rm $WORKSPACE/DB_backups/archive/data_from_gr/gr_data_original_lv_$date.tar.gz
#
#
##writing latest backup date
echo $date > $WORKSPACE/DB_backups/archive/.last_copied.txt


