#!/bin/bash
# Yaniv Costica
# yaniv.costica@waycaretech.com

# Follow the steps on this link:
# https://gist.github.com/ushu/7217693
# Note the following comment:
#To make it work I had to also give access to source-bucket/*
#Like this :
#"Resource": [
#"arn:aws:s3:::source-bucket",
#"arn:aws:s3:::source-bucket/*"
#]

# Generally the steps are:
# 1) Add access key at destination account
# 2) Edit policy at source bucket
# 3) Log to an instance with s3cmd installed with destination account configuration
# 4) As we  want to do it safetly, I did this script as semi-automated.
# run the following command to get the list of directories of the source bucket:
# s3cmd ls s3://<source-bucket>
# create a  list of dirs out of the dirs list that you want to copy (list name should be named "list_of_elements_to_copy" )
# 5) Run the script from there

source_bucket="s3://waycare-data-original-bkp/"
destination_bucket="s3://waycare-data-original-bkp-encrypted/"
base_folder='archive/'
destination_folder=''

list_lv_all=("data_from_geotab/" "data_from_geotab_bgcsnv/" "data_from_geotab_lvrtc/" "data_from_geotab_rtcsnv/" "data_from_geotab_trips/" "data_from_wu/" "data_from_wu_forecast/" "data_from_lvmpd_lv/" "data_from_tm/" "data_from_twitter_lv/" "data_from_lv_conventions/" "data_from_cmwg_construction/" "data_from_fast_dms/" "data_from_wz/" "data_from_wz_jam/" "data_from_wz_irregularities/" "data_from_fast_sensors/" "data_from_ndot/" "data_from_ndot_sp/" "data_from_cisco_mqtt/" "data_from_here/" "data_from_otonomo/" "data_from_gr/" "data_from_nhp/" "data_from_nhp_loc/" "data_from_traffic_lights_lv/")
list_tampa_all=("data_from_8channel_tampa/" "data_from_rc_tampa/" "data_from_tm_tampa/" "data_from_twitter_tampa/" "data_from_wu_forecast_tampa/" "data_from_wz_irregularities_tampa/" "data_from_wz_jam_tampa/" "data_from_wz_tampa/")
list_tampa_2=("data_from_bt_sensors/" "data_from_fl511/" "data_from_tmc_road_closures/" "data_from_tmc_road_incidents/" "data_from_intouch_gps/" "data_from_tampa_bt_sensors/")
list_pinellas_all=("data_from_seeclickfix/" "data_from_bt_sensors_pinellas/" "data_from_pinellas_construction_pinellas/" "data_from_twitter_pinellas/" "data_from_wu_forecast_pinellas/" "data_from_wu_pinellas/" "data_from_wz_jam_pinellas/" "data_from_wz_pinellas/")
list_delaware_all=("data_from_delaware_weather_districts/" "data_from_delaware_weather_hydrology_levels/" "data_from_delaware_weather_snow_totals/" "data_from_delaware_weather_surface/" "data_from_wu_forecast_delaware/" "data_from_travel_restrictions/" "data_from_traveltimes/" "data_from_noaa")
list_of_elements_to_copy=("pinellascounty/" "lv/")



for element_to_copy in "${list_of_elements_to_copy[@]}"; do
    source_element_full_path=${source_bucket}${base_folder}${element_to_copy}
    destination_element_full_path=${destination_bucket}${base_folder}${destination_folder}${element_to_copy}
    echo "---------"
    echo "s3cmd cp --recursive ${source_element_full_path} ${destination_element_full_path}"
    echo "---------"
    s3cmd cp --recursive ${source_element_full_path} ${destination_element_full_path}
done


