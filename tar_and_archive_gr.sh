#setting date to run
if [ -z "$1" ]
  then
    echo "No argument supplied- please run: sh tar_and_archive.sh [dd_mm_yyyy or auto]"
    exit 1
fi

if [ $1 = "auto" ];
  then
    date="$(date -d "-1 days" +'%d_%m_%Y')"
else
    date="$1"
fi

# checking if already back up
file="$WORKSPACE/DB_backups/archive/.last_copied_gr.txt"
if [ ! -f "$file" ]
  then
    echo "$0: File '${file}' not found."
  else
    while IFS= read -r line
    do
        # display $line or do somthing with $line
        #echo "$line"
        #echo "$date"
        if [ $line = $date ];
          then
            printf '%s has already been archived, exiting\n' "$line"
            exit 1
        fi
    done <"$file"
fi

echo "starting backups for date" $date
echo $WORKSPACE/DB_backups/




#data_from_gr
tar -cvzf $WORKSPACE/DB_backups/archive/data_from_gr/gr_live_lv_$date.tar.gz  $WORKSPACE/DB_backups/data_from_gr/database_original/*$date.txt
rm $WORKSPACE/DB_backups/data_from_gr/database_original/*$date.txt



#####################################
#archiving in s3:
s3cmd put $WORKSPACE/DB_backups/archive s3://waycare-datastorage-1/ --recursive

#removing local copies
rm $WORKSPACE/DB_backups/archive/data_from_gr/gr_live_lv_$date.tar.gz

#writing latest backup date
echo $date > $WORKSPACE/DB_backups/archive/.last_copied_gr.txt



