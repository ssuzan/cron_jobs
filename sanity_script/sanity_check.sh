#!/bin/bash

# Yaniv Costica
# yaniv.costica@waycaretech.com

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# Check what is the instance IP:
ifconfig_res=`ifconfig eth0 2>/dev/null`
echo $if_res
if [ ! -z "${ifconfig_res}" ]; then
	IP=`ifconfig eth0 2>/dev/null | awk '/inet addr:/ {print $2}' | sed 's/addr://'`
else
	IP=`ifconfig ens5 2>/dev/null | awk '/inet addr:/ {print $2}' | sed 's/addr://'`
fi
if [ -z "${IP}" ]; then
	IP=`ifconfig ens5 | grep "inet " | sed 's!.*inet !!' | sed 's!  netmask.*!!'`
fi

SPACE_PERCENTAGE_ALERT=90
SPACE_GB_ALERT=15
SECONDS_IN_DAY=86400
SECONDS_IN_HOUR=3600
PATH_TO_DATA="${WORKSPACE}"

report_name=`date +sanity_report_%Y_%m_%d_%H_%M_%S.txt`
report_file="${DIR}/${report_name}"
not_active_services=()
not_active_services_status=()
data_folders_not_up_to_date=()

email_file="${WORKSPACE}/serverscripts_production/globals/emails"
export email_file
email_address=`cat ${email_file} | sed 's!.*\"email\": \"!!' | sed 's!\"\,\ \"password\".*!!'`
export email_address
email_password=`cat ${email_file} | sed 's!\"comments\".*!!' | sed 's!.*\"password\"\: \"!!' | sed 's!\", \"send_emails.*!!'`
export email_password
email_destination=`cat ${email_file} | sed 's!\"comments\".*!!' | sed 's!.*\"send_emails\"\: !!' | sed 's!],.*!]!'`
export email_destination

# Delete previous reports:
find ${DIR} | grep "sanity_report" | xargs rm -v >/dev/null

# NOTE: This is how to fetch all 'data_from_*' folders (do it from each machine and set "database_original_dirs" accordingly):
# ll "${WORKSPACE}/DB_backups/" | sed 's!.*data_from_!!g' | sed 's!/!!'
# - services_list: list of all the services
# - database_original_dirs: list of all the dir's names in DB_backups folder and the maximum hours that can pass since last file arrived to the database_original folder
### LV
if [[ "${IP}" == "10.0.3.221" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/lv.config"
	platform="LV"
### LV Dev:
elif [[ "${IP}" == "10.0.3.190" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/lv_dev.config"
	platform="LV Dev"
### LV Demo:
elif [[ "${IP}" == "10.0.3.241" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/lv_demo.config"
	platform="LV Demo"
### Tampa:
elif [[ "${IP}" == "10.0.3.100" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/tampa.config"
	platform="Tampa"
### Walnut Creek
elif [[ "${IP}" == "10.0.3.153" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/walnut_creek.config"
	platform="Walnut Creek"
### Delaware
elif [[ "${IP}" == "10.0.3.79" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/delaware.config"
	platform="Delaware"
### Pinellas County
elif [[ "${IP}" == "10.0.3.171" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/pinellas_county.config"
	platform="Pinellas"
### North Nevada
elif [[ "${IP}" == "10.0.3.33" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/n_nv.config"
	platform="North Nevada"
### Verizon N_NV (Snow Plows Data)
elif [[ "${IP}" == "10.6.1.40" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/verizon.config"
	platform="Verizon (N_NV Snow Plows)"
### Henderson
elif [[ "${IP}" == "10.0.3.176" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/henderson.config"
	platform="Henderson"
### Hiilsborough County
elif [[ "${IP}" == "10.0.3.168" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/hillsborough_county.config"
	platform="Hiilsborough"
### COTA
elif [[ "${IP}" == "10.5.20.100" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/cota.config"
	platform="COTA"
### GreenRoad Machine
elif [[ "${IP}" == "10.0.3.107" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/gr.config"
	platform="GreenRoad Machine"
### Output Server
elif [[ "${IP}" == "10.0.3.42" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/output_server.config"
	platform="Output Server"
### Wejo Invehicle Server
elif [[ "${IP}" == "10.6.0.60" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/wejo.config"
	platform="Wejo Invehicle Server"
### Mojio Invehicle Server
elif [[ "${IP}" == "10.0.7.200" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/mojio.config"
	platform="Mojio Invehicle Server"
### Nexar Invehicle Server
elif [[ "${IP}" == "10.6.0.153" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/nexar_invehicle.config"
	platform="Nexar Invehicle Server"
### Otonomo Invehicle Server
elif [[ "${IP}" == "10.6.0.45" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/otonomo.config"
	platform="Otonomo Invehicle Server"
### Main Invehicle Server
elif [[ "${IP}" == "10.6.0.20" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/invehicle_server.config"
	platform="Main Invehicle Server"
#################
### Waycare 2 ###
#################
### Missouri Waycare2
elif [[ "${IP}" == "172.16.31.5" ]]; then
	source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/wc2_missouri.config"
	platform="WC2: Missouri"
else
	echo "Unrecognized IP. Aborting..." >> ${report_file}
	exit 1
fi

echo "detected platform: ${platform}"

echo "Sanity Check For ${platform} Instance: ${IP}" > ${report_file}
echo "" >> ${report_file}

###
# 1. Check services:
###
alert_services=false
for service in "${services_list[@]}"
do
	is_active=`systemctl is-active $service`
	if [[ "${is_active}" != "active" ]]; then
		alert_services=true
		echo "Service $service is ${is_active}" >> ${report_file}
		#status=`systemctl status $service`
		#not_active_services_status+=status
	fi
done

echo "" >> ${report_file}

###
# 2. Check available disk space
###
alert_disk_space=false
# 2.1 Check space in GB
space_avail=`df --output=avail -h "${PWD}" | sed '1d;s/[^0-9]//g'`
if [ ${space_avail} -lt ${SPACE_GB_ALERT} ]; then
	alert_disk_space=true
fi
# 2.2 Check space in percentages
#disk_space_in_percentages=`df -kh . | grep '/dev/xvda1' | sed 's!.*G!!g' | sed 's!%.*!!'`
disk_space_in_percentages=$((`df -kh . | grep '/dev/' | sed 's!.*G!!g' | sed 's!%.*!!'`+0))
if [ $disk_space_in_percentages -ge ${SPACE_PERCENTAGE_ALERT} ]; then
	alert_disk_space=true
fi
echo "Space available on the machine: ${space_avail}GB which is $((100-disk_space_in_percentages))%" >> ${report_file}
echo "" >> ${report_file}
###
# 3. Check time of last file in folder
###
alert_last_modified=false
num_of_element=${#database_original_dirs[@]}
num_of_folders=$((num_of_element/2))
now_unix_time=`date +%s`
for i in `seq 0 $((num_of_folders-1))`; do
    folder_name_index=$((2*i))
	dir="${database_original_dirs[$folder_name_index]}"
	full_path="${PATH_TO_DATA}/${dir}"
	last_file_unix_time=`date +%s -r ${full_path}`

	max_time_from_last_file_index=$((2*i+1))
	max_time_from_last_file=$((SECONDS_IN_HOUR*database_original_dirs[$max_time_from_last_file_index]))
	diff_time_in_mins=$(((now_unix_time-last_file_unix_time)/60))

	if [ $((now_unix_time-last_file_unix_time)) -gt ${max_time_from_last_file} ]; then
		alert_last_modified=true
		if [ ${diff_time_in_mins} -lt 60 ]; then
			echo "Last file in ${full_path} is from: ${diff_time_in_mins} minutes ago" >> ${report_file}
		else
			echo "Last file in ${full_path} is from: $((diff_time_in_mins/60)) hours ago" >> ${report_file}
		fi
	fi
done

if [ ${alert_services} == true ] || [ ${alert_disk_space} == true ] || [ ${alert_last_modified} == true ];then
	subject="Failed: Sanity Script For ${platform}"
else
	subject="Success: Sanity Script For ${platform}"
fi
export subject

echo ${subject}
cat "${report_file}"
report_file_text=`cat ${report_file}`
export report_file_text

IFS='"' read -ra ADDR <<< "$email_destination"
# for i in "${ADDR[@]}"; do
# 	if [[ ${i} ==  *"com"* ]]; then
email="\"$i\""
export email
echo "Send to ${email}"
python3 -c'import sys,os;sys.path.append(os.environ["WORKSPACE"] + "/serverscripts_production/globals");import sendemail;sendemail.sendemail(from_addr=os.environ["email_address"],to_addr_list=os.environ["email"],cc_addr_list=[],subject=os.environ["subject"],message=os.environ["report_file_text"],login=os.environ["email_address"],password=os.environ["email_password"])'
# 	fi
# done

