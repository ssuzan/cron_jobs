Date: 15.05.2018

For every new script:
1. Add the service name to the service section (services_list) at the relevant platform's config file.
2. Add the name of the data folder and the number of hours*
	* When the script runs, it checks the time since the last file was added to database_original.
	  If this time is greater then the number of hours you've entered at  the config file, it will alert in email.

For every new platform:
1. Add new config  file with the relevant confidurations.
2. Add new 'elif' section at the beginning of the sanity script that checks the IP address and uses the new config in case of a match.
