# Yaniv Costica
# yaniv.costica@waycaretech.com

import sys,os
import os.path
import subprocess

dire = os.path.dirname(__file__)
sanity_file = os.environ['WORKSPACE']+'/cron_jobs/sanity_script/sanity_check.sh'

def sanity_call_all():
	res = subprocess.check_output([sanity_file])
	print ('res is:')
	print (res)

if __name__ == "__main__":
	sanity_call_all()


