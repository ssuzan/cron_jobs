# Yaniv Costica
# yaniv.costica@waycaretech.com

import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import apscheduler
from apscheduler.schedulers.blocking import BlockingScheduler
import sanity_call_all

def scheduled_call():
    print('sanity_check_scheduled_call started successfuly!')
    # Initialize new scheduler
    sched = BlockingScheduler(timezone='UTC',coalesce=True)
    sched.add_job(sanity_call_all.sanity_call_all, 'cron', id='job_id1', hour='18',coalesce=True)
    #sched.add_job(sanity_call_all.sanity_call_all, 'interval', id='job_id1', minutes=1, coalesce=True)
    sched.start()

scheduled_call()
