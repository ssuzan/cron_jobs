#Ethan Gellman
#esgellman@hotmail.com 954-610-6868
#main file
#required installations:
import sys
import os
workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
import apscheduler
from apscheduler.schedulers.blocking import BlockingScheduler


def tar_and_archive_nhp():
    cmd= '/bin/sh /home/ubuntu/workspace/cron_jobs/tar_and_archive_nhp.sh auto >> /home/ubuntu/workspace/cron_jobs/cron_log.log &'
    os.system(cmd)
    print('tar_and_archive_nhp completed successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')


def scheduled_call():
    print('archive_nhp_scheduled_call started successfuly! time is: '+universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S")+' UTC')
    #initialize new scheduler
    sched = BlockingScheduler(timezone='UTC',coalesce=True)
    sched.add_job(tar_and_archive_nhp, 'cron', id='job_id1', hour='9', minute='5',coalesce=True)
    sched.start()


scheduled_call()
