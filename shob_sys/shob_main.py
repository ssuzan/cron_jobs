### IMPORTS:
import os
import sys
import json
import pymongo
from datetime import datetime, timedelta
import pprint

# NOTE: script has to run on an instance that is running on UTC time. Else, please update timezone of datetime.now() to UTC.

### CONSTANTS:
WORKSPACE_ENV = str(os.environ['WORKSPACE'])
MAPPING_FILE_PATH = WORKSPACE_ENV + '/cron_jobs/shob_sys/collections_mapping.json'

# Load the data about each platform, it's DB connectivity information and collections
with open (MAPPING_FILE_PATH) as f:
    db_info = json.load(f)

host = '10.5.10.100'
db_name = 'windbear'
port = 27017

mongo_client = pymongo.MongoClient(host=host, port=port)

# Get list of DBs
listOfDBs = mongo_client.database_names()

# Get list of Collections
db = mongo_client['windbear']
listOfCollections = db.collection_names()

# Check if time since last check is OK
# There are four different status:
# 0: Valid
# 1: Almost Valid (A bit more than it should)
# 2: Not Valid
# 3: Error occure during compare_time function
def compare_time(time_dt, now, collection_interval=24):
    try:
        status_0 = now - timedelta(hours=collection_interval)
        status_1 = now - timedelta(hours=(2*collection_interval))
        if time_dt > status_0:
            status = 0
        elif status_0 < time_dt < status_1:
            status = 1
        else:
            status = 2
        time_str = time_dt.strftime("%d_%m_%Y_%H_%M_%S")
        return [time_str, status]
    except Exception as e:
        print ('Error: ' + str(e))
        return ["Unknown", 3]

output_list = []

#     output_json = {}
#     output_json[platform_name] = {}
# for collection in listOfCollections:
#     print ('\n')
#     print (collection)
#     last_element_received_in_collection = db[collection].find({}).sort('_id', pymongo.DESCENDING).limit(1)
#     last_element_dt = last_element_received_in_collection[0]['_id'].generation_time.replace(tzinfo=None)
#     now = datetime.now()
#     time_str, status = compare_time(last_element_dt, now)
#     print ('last_received: ' + str(time_str))
#     print ('status: ' + str(status))
#     print ('time_of_check: ' + str(now.strftime("%d_%m_%Y_%H_%M_%S")))
