import os
import json_to_csv_tt


def main(convertor_marker, list_of_files, output_directory, do_delete_input=True):
    # First, make sure that the output folder exists, and if not - create it:
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)
    # Now, check which convertor should be applied:
    if convertor_marker == 'travel_times':
        json_to_csv_tt.main(list_of_files, output_directory, do_delete_input)
        return True
    else:
        print ("convertor_marker: " + str(convertor_marker) + " is not recognized")
        return False