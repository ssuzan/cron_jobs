import pandas as pd
import json
from copy import deepcopy
import os
import traceback

def update_dict(origin_dict, prefix):
    """Add prefix to the dict key.
    
    Arguments:
        origin_dict {dict} -- The input dict.
        prefix {dict} -- The prefix.

    Returns:
        [dict] -- The dict with updated key.
    """
    return {prefix + key: val for key, val in origin_dict.items()}


def unpack_unique_fields(main_data):
    """Extract data from 'unique_fields'.
    
    Arguments:
        main_data {dict} -- The input dict.

    Returns:
        [dict] -- The dict with updated key.
    """
    # Copy the main dict
    input_data = deepcopy(main_data)
    # Set the prefix
    prefix = 'unique_fields.'
    # Get the 'unique_fields'
    unique_fields = input_data.get('unique_fields', {})
    # Extract key, value from 'unique_fields'
    unique_fields_unp = update_dict(unique_fields, prefix)
    
    if unique_fields:
        # Add new data to the copy dict
        input_data.update(unique_fields_unp)
        # Remove the 'unique_fields'
        del input_data['unique_fields']
    
    return input_data


def unpack_location(main_data):
    """Extract data from 'location'.
    
    Arguments:
        main_data {dict} -- The input dict.

    Returns:
        [dict] -- The dict with updated key.
    """
    # Copy the main dict
    input_data = deepcopy(main_data)
    # Set the prefix
    start_prefix = 'location.start_coord.'
    end_prefix = 'location.end_coord.'
    # Get the 'location'
    start_coord = input_data.get('location', {}).get('start_coord', {})
    end_coord = input_data.get('location', {}).get('end_coord', {})
    # Extract key, value from 'location'
    start_coord_unp = update_dict(start_coord, start_prefix)
    end_coord_unp = update_dict(end_coord, end_prefix)

    if any([start_coord, end_coord]):
        # Add new data to the copy dict
        input_data.update(start_coord_unp)
        input_data.update(end_coord_unp)
        # Remove the 'unique_fields'
        del input_data['location']
    
    return input_data


def main(list_data, path_to_csv, do_delete_input=True):
    """Convert json and save to csv.
    Arguments:
        list_data {list} -- The input list.
        path_to_csv {string} -- The path where the csv file will be saved.
    """
    for file_path in list_data:
        try:
            file_name = file_path.split('/')[-1].split('.')[0]
            output_csv_file = path_to_csv + file_name + '.csv'
            with open(file_path) as f:
                parsed_data = json.load(f)
            data = parsed_data.get('data')
            print('Starting for ' + file_name + '...')
            #print('  Extracting data from `unique_fields`')
            # Extract data from 'unique_fields'
            new_data = list(map(unpack_unique_fields, data))
            #print('  Extracting data from `location`')
            # Extract data from 'location'
            new_data = list(map(unpack_location, new_data))
            # Open data as DataFrame
            df = pd.DataFrame(new_data)
            # Get the columns names
            columns_names = list(df.columns)
            columns_names.remove('waycare_id')
            # Explode the 'waycare_id' column
            new_df = df.set_index(columns_names)['waycare_id'].apply(pd.Series).stack().reset_index().rename(columns={0: 'waycare_id'})
            # Drop the level column
            new_df.drop(new_df.filter(regex='level_').columns, axis=1, inplace=True)
            print('  Saving CSV')
            # Start index from 1
            new_df.index = new_df.index + 1
            # Save the data to CSV file
            new_df.to_csv(output_csv_file, index_label='index')
            print('Done!')
        except Exception as err:
            print ("Error in json_to_csv_tt.main! " + str(err))
            error_message = traceback.format_exc()
            print ("\nError!\n" + error_message)


##################################### TEST #####################################
#path = "./input_file.json"
#with open(path) as f:
#    data = json.load(f)
#main(data, './output_file.csv')
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ TEST ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^#
