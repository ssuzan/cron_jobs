#!/bin/bash

# Yaniv Costica
# yaniv.costica@waycaretech.com
# Dec 12, 2018

# Script Summary:
# 1) Import the conf_file (conf file is the input parameter)
# 2) Setting date to run
# 3) Checking if already back up
# 4) Iterate on each data source and add it to backup
    # 4.1) Extract from "sources" list the folder name and data source name
    # 4.2) Get all files of the relevant date
    # 4.3) ZIP all the files we want to backup
    # 4.4) Archiving to S3
    # 4.5) Archiving to glacier
    # 4.6) Cleaning up


# 1) conf_file include two things:
# - List including all the data sources we want to backup
# - Glacier vault name
conf_file=${2}
echo "conf_file is: ${conf_file}"
source ${conf_file}

# 2) Setting date to run
script_name=`basename "$0"`
if [ -z "$1" ]; then
    echo "No argument supplied - please run: sh ${script_name} [dd_mm_yyyy or auto]"
    exit 1
fi
if [ $1 = "auto" ]; then
    # Option 1: default (Yesterday)
    date="$(date -d "-1 days" +'%d_%m_%Y')"
else
    # Option 2: input date
    date="$1"
fi

# 3) Checking if already back up
file="$WORKSPACE/DB_backups/archive/.last_copied.txt"
if [ ! -f "$file" ]; then
    echo "$0: File '${file}' not found."
else
    while IFS= read -r line
    do
        if [ $line = $date ]; then
            printf '%s has already been archived, exiting\n' "$line"
            exit 1
        fi
    done <"$file"
fi
echo "Starting backups for date: ${date}"
ifconfig_res=`ifconfig eth0 2>/dev/null`
if [ ! -z "${ifconfig_res}" ]; then
	IP=`ifconfig eth0 2>/dev/null | awk '/inet addr:/ {print $2}' | sed 's/addr://'`
else
	IP=`ifconfig ens5 2>/dev/null | awk '/inet addr:/ {print $2}' | sed 's/addr://'`
fi

zip_base_name="${date}_${platform_name}_${IP}"
archive_base_folder_name="$WORKSPACE/DB_backups/archive/${zip_base_name}"
mkdir -p ${archive_base_folder_name} # TODO: delete it


# 4) Iterate on each data source and add it to backup:
for source_name in "${!sources[@]}"
do
    # 4.1) Extract from "sources" list the folder name and data source name
    echo ""
    echo "Source_name is: $source_name"
    data_folder="${sources[$source_name]}"
    echo "data_folder is: ${data_folder}"
    echo ""
    manifest_file="/tmp/test_${platform_name}.manifest"
    # 4.2) Get all files of the relevant date
    find $WORKSPACE/DB_backups/${data_folder}/database_original | grep $date  > ${manifest_file}
    temp=$(cat ${manifest_file} | wc -l)
    if [ $temp -gt 0 ]
    then
        # 4.3) ZIP all the files we want to backup:
        mkdir -p ${archive_base_folder_name}/${data_folder}
        if tar -cvzf ${archive_base_folder_name}/${data_folder}/${source_name}_${date}.tar.gz  --files-from ${manifest_file}
        then
            echo "tar success for ${source_name}"
            # 4.4) Archiving to S3:
            echo "Archiving to S3 - ${source_name}:"
            if s3cmd --server-side-encryption put ${archive_base_folder_name}/${data_folder}/${source_name}_${date}.tar.gz s3://${bucket_name}/archive/${platform_name}/${data_folder}/ --recursive
            then
                # 4.5) Cleaning up:
                echo "Deleting files from source - ${source_name} With date of: ${date}"
                find $WORKSPACE/DB_backups/${data_folder}/database_original | grep $date | xargs rm -v
            else
                echo "${source_name} S3 archive failed"
            fi
        else # Failed
            echo "${source_name} ZIP failed"
        fi
    fi
done

# 5) Tar to backup S3 bucket zip of zips (one zip of all data at this platform\region)
# 5.1) Tar&ZIP all of the zips
echo "Archiving to Backup S3 Bucket- ${platform_name}:"
if tar -cvzf ${archive_base_folder_name}/../${zip_base_name}.tar.gz  ${archive_base_folder_name}
then
    echo "Tar success for ${platform_name}"
    # Archiving to Backup S3 Bucket
    ${archive_base_folder_name}/../${zip_base_name}.tar.gz
    
    # 5.2) Archiving to S3:
    echo "Archiving to S3 - ${platform_name}:"
    if s3cmd put --server-side-encryption ${archive_base_folder_name}/../${zip_base_name}.tar.gz s3://${backup_bucket_name}/archive/${platform_name}/"combined_data"/ --recursive
    then
        echo "Upload data to S3 - ${platform_name} success"
    else
        echo "${platform_name} S3 archive failed"
    fi
else # Failed
    echo "Main ZIP ${zip_base_name}.tar.gz ZIP failed"
fi


#if tar -cvzf ${archive_base_folder_name}/../${zip_base_name}.tar.gz  ${archive_base_folder_name}
#then
#    # Archiving to glacier:
#    echo "Archiving to glacier- ${zip_base_name}:"
#    if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v ${glacier_name} -f ${archive_base_folder_name}/../${zip_base_name}.tar.gz -d "${zip_base_name}"
#    then # Success
#        placeholder=1
#    else # Failed
#        echo "${zip_base_name} glacier archive failed"
#    fi
#else # Failed
#    echo "Main ZIP ${zip_base_name}.tar.gz ZIP failed"
#fi

#
echo "Deleting zips from date: ${date} (Folder: ${archive_base_folder_name})"
rm -rf ${archive_base_folder_name}
rm $WORKSPACE/DB_backups/archive/${zip_base_name}.tar.gz
