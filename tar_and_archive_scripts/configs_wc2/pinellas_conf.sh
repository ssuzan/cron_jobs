#!/bin/bash

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="pinellas_county"

# Note carefully to the structure!
declare -A sources=( \
["original_bt_sensors_${platform_name}"]="data_from_pinellas_bt_sensors" \
["original_pinellas_911_incidents_${platform_name}"]="data_from_pinellas_incidents" \
["original_wz_alerts_${platform_name}"]="data_from_wz_alerts" \
["original_wz_jams_${platform_name}"]="data_from_wz_jams" \
["original_climacell_${platform_name}"]="data_from_climacell" \
["original_pinellas_road_closures_${platform_name}"]="data_from_pinellas_road_closures" \
["original_fl511_dms_${platform_name}"]="data_from_fl511_dms" \
["original_here_speed_segments_${platform_name}"]="data_from_here_speed_segments" \
["original_here_fhp_incidents_${platform_name}"]="data_from_fhp_incidents" \
["original_here_fdot_construction_${platform_name}"]="data_from_fdot_construction_pinellas" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name="waycare_${platform_name}_original_data_bkp"

# Main bucket name:
bucket_name="prod-waycare-data-backup"

# Backup bucket name:
backup_bucket_name="prod-waycare-data-backup-2nd"