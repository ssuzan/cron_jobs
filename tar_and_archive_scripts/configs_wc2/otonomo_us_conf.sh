#!/bin/bash

# Note carefully to the structure!
declare -A sources=( \
["otonomo_cv_original"]="data_from_otonomo_cv" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name="waycare_us_original_data_bkp"

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="us"

# Main bucket name:
bucket_name="prod-waycare-data-backup"

# Backup bucket name:
backup_bucket_name="prod-waycare-data-backup-2nd"