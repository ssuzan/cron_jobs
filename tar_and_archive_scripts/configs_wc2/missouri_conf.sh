#!/bin/bash

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="missouri"

# Note carefully to the structure!
declare -A sources=( \
["original_wz_alerts_${platform_name}"]="data_from_wz_alerts" \
["original_wz_jams_${platform_name}"]="data_from_wz_jams" \
["original_climacell_${platform_name}"]="data_from_climacell" \
["original_missouri_dms_${platform_name}"]="data_from_missouri_dms" \
["original_missouri_sensors_metadata_${platform_name}"]="data_from_missouri_sensors_metadata" \
["original_missouri_sensors_${platform_name}"]="data_from_missouri_sensors" \
["original_geotab_missouri_transportation_${platform_name}"]="data_from_geotab_missouri_transportation" \
["original_here_speed_segments_${platform_name}"]="data_from_here_speed_segments" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name="waycare_${platform_name}_original_data_bkp"

# Main bucket name:
bucket_name="prod-waycare-data-backup"

# Backup bucket name:
backup_bucket_name="prod-waycare-data-backup-2nd"