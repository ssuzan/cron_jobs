#!/bin/bash

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="ctrma"

# Note carefully to the structure!
declare -A sources=( \
["original_wz_alerts_${platform_name}"]="data_from_wz_alerts" \
["original_wz_jams_${platform_name}"]="data_from_wz_jams" \
["original_climacell_${platform_name}"]="data_from_climacell" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name="waycare_${platform_name}_original_data_bkp"

# Main bucket name:
bucket_name="prod-waycare-data-backup"

# Backup bucket name:
backup_bucket_name="prod-waycare-data-backup-2nd"