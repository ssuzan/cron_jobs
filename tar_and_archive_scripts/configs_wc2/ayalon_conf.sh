#!/bin/bash

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="il"

# Note carefully to the structure!
declare -A sources=( \
["original_wz_alerts_${platform_name}"]="data_from_wz_alerts" \
["original_wz_jams_${platform_name}"]="data_from_wz_jams" \
["original_climacell_${platform_name}"]="data_from_climacell_rt" \
["original_pointer_ayalon_service_patrol_${platform_name}"]="data_from_pointer_ayalon" \
["original_buses_il_trips"]="data_from_buses_il_trips" \
["original_buses_il_trips"]="data_from_buses_il_locations" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name="waycare_${platform_name}_original_data_bkp"

# Main bucket name:
bucket_name="prod-waycare-data-backup"

# Backup bucket name:
backup_bucket_name="prod-waycare-data-backup-2nd"