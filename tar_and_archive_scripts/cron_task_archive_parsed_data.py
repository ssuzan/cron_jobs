# Yaniv Costica
# yaniv.costica@waycaretech.com
# Cron file for tar and archive.
# Should be called with 1 input parameter: configuration file (example: "configs_parsed_data/wz_tt_lv.sh")


import sys
import os, shutil
import glob
from datetime import date, timedelta
import traceback
dire = os.path.dirname(os.path.abspath(__file__))
workspace_environ = str(os.environ['WORKSPACE'])    
import apscheduler
from apscheduler.schedulers.blocking import BlockingScheduler
from zipfile import ZipFile
import subprocess

import tar_and_archive_main
sys.path.append(workspace_environ + "/serverscripts_production/globals")
import universal_imports
sys.path.append(workspace_environ+"/cron_jobs/tar_and_archive_scripts/json_convertors")
import convertors_manager

def clean_folder(full_folder_path):
    for the_file in os.listdir(full_folder_path):
        file_path = os.path.join(full_folder_path, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            #elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            error_message = traceback.format_exc()
            print ("Error!\n" + error_message)

# Zip the files from given directory <files_path> (full path)
# that matches the <pattern>
# with the extention of <extention>
# to a zip file name <output_zip_file_name> (full path)
def zipFilesInDir(files_path, output_zip_file_name, pattern, extention='csv'):
    # create a ZipFile object
    print ('zip file path: ' + files_path + output_zip_file_name)
    with ZipFile(files_path + output_zip_file_name, 'w') as zipObj:
        # Iterate over all the files in directory
        print ('look for files in: ' + files_path)
        for folderName, subfolders, filenames in os.walk(files_path):
            print ("filenames: " + str(filenames))
            print ("pattern: " + str(pattern))
            for filename in filenames:
                if (pattern in filename) and (extention in filename):
                    # create complete filepath of file in directory
                    filePath = os.path.join(folderName, filename)
                    # Add file to zip
                    zipObj.write(filePath)


def tar_and_archive_for_parsed_data(config_file, pattern_for_archive='auto'):
    # 1) get config info:
    config_file = dire + '/' + config_file
    with open(config_file) as json_data_file:
        config_data = universal_imports.json.load(json_data_file)
    platform_name = config_data.get("platform_name")
    bucket_name = config_data.get("bucket_name")
    glacier_name = config_data.get("glacier_name")
    for data_source_to_convert in config_data.get("data_sources_to_archive"):
        # Load data source configuration parameters:
        data_source = data_source_to_convert.get("data_source")
        print ("data_source_to_convert is: " + data_source)
        delete_input_files = data_source_to_convert.get("delete_input_files")
        input_directory_path = workspace_environ + data_source_to_convert.get("input_directory_path_from_workspace")
        input_data_extention = data_source_to_convert.get("input_data_extention")
        output_directory_path = workspace_environ + data_source_to_convert.get("output_directory_path_from_workspace")
        convertor_marker = data_source_to_convert.get("convertor_marker")
        # 2) pattern of files to convert:
        if pattern_for_archive == 'auto':
            yesterday = date.today() - timedelta(days=1)
            pattern = yesterday.strftime('%d_%m_%Y_00')
        else:
            pattern = pattern_for_archive
        output_zip_name = data_source + '_' + pattern + '.zip'
        pattern_in_file_name = input_directory_path + "*" + pattern + "*." + input_data_extention
        print ("pattern_in_file_name is: " + pattern_in_file_name)
        # 3) Get all files with pattern into a list:
        all_files_to_archive = []
        for _file in glob.glob(pattern_in_file_name):
            try:
                all_files_to_archive.append(_file)
            except:
                error_message = traceback.format_exc()
                print ("Error!\n" + error_message)
        if not all_files_to_archive: # Check if the list is empty
            print ('No files to archive for data source ' + data_source + ' at platform: ' + platform_name)
            continue
        # 4) Convert the JSONs to CSVs
        convertors_manager.main(convertor_marker, all_files_to_archive, output_directory_path, delete_input_files)
        # 5) Convert csv files to zip:
        zipFilesInDir(output_directory_path, output_zip_name, pattern)
        # 6) Arcive the zip file:
        cmd_upload_to_s3 = 's3cmd put ' + output_directory_path + output_zip_name + ' s3://' + bucket_name + '/fllaten_parsed_data/' + platform_name + '/' + data_source + '/ --recursive' 
        print ('Executing: ' + cmd_upload_to_s3)
        #os.system(cmd_upload_to_s3)
        process = subprocess.Popen(cmd_upload_to_s3.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        print (output, error)
        # 7) Delete zip and flatten files
        clean_folder(output_directory_path)
        print("ZIP and flattens Removed!")
        print('tar_and_archive_for_parsed_data called with ' + config_file + ' completed successfuly! time is: ' + universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S") + ' UTC')
        # Delete the parsed files in case needed:
        if delete_input_files:
            print ("Deleting parsed files")
            for file_path in all_files_to_archive:
                os.remove(file_path)


def scheduled_call(conf_file, pattern):
    print('archive_scheduled_call started successfuly! time is: ' + universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S") + ' UTC')
    #initialize new scheduler
    sched = BlockingScheduler(timezone='UTC',coalesce=True)
    sched.add_job(tar_and_archive_for_parsed_data, 'cron', id='job_id1', hour='9', minute='5',coalesce=True, args=[conf_file, pattern])
    sched.start()

try:
    if len(sys.argv) > 1:
        config_file = sys.argv[1]
        pattern = sys.argv[2]
        is_schedule_run = sys.argv[3]
        if is_schedule_run == '1':
            scheduled_call(config_file, pattern)
        else:
            tar_and_archive_for_parsed_data(config_file, pattern)
except:
    error_message = traceback.format_exc()
    print ("Error!\n" + error_message)
    print (
        '\nFailed to run.\n\
        Run:\n\tpython3 cron_task_archive_parsed_data.py <config_file> <pattern> <is_schedule_run>\n\
        Or to run one time from python3:\n\ttar_and_archive_for_parsed_data.tar_and_archive_for_parsed_data(<conf_file>, <pattern_for_archive>)\n\
        * pattern_for_archive should be "auto" for yesterday date as pattern\n\
        * <is_schedule_run> is 0 for False 1 for True'
        )


