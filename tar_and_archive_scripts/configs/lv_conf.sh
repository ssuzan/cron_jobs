#!/bin/bash

# Note carefully to the structure!
declare -A sources=( \
["geotab_original_live_lv"]="data_from_geotab" \
["geotab_trips_lv"]="data_from_geotab_trips" \
["geotab_lvrtc_lv"]="data_from_geotab_lvrtc" \
["geotab_bgcsnv_nevada"]="data_from_geotab_bgcsnv" \
["geotab_rtcsnv_nevada"]="data_from_geotab_rtcsnv" \
["climacell_rt_lv"]="data_from_climacell_rt" \
["climacell_forecast_lv"]="data_from_climacell_forecast" \
["fast_dms_original"]="data_from_fast_dms" \
["waze_traffic_alerts_original_lv"]="data_from_wz" \
["waze_traffic_jam_original_lv"]="data_from_wz_jam" \
["waze_traffic_irregularities_original_lv"]="data_from_wz_irregularities" \
["sensors_data_original_lv"]="data_from_fast_sensors" \
["ndot_original_live_lv"]="data_from_ndot" \
["ndot_sp_original_live_lv"]="data_from_ndot_sp" \
["tm_original_live_lv"]="data_from_tm" \
["twitter_original_lv"]="data_from_twitter_lv" \
["lvmpd_lv"]="data_from_lvmpd_lv" \
["lv_conventions_lv"]="data_from_lv_conventions" \
["cmwg_construction_lv"]="data_from_cmwg_construction" \
["icone_construction_lv"]="data_from_icone_construction" \
["icone_sensors_lv"]="data_from_icone_sensors" \
["cisco_mqtt_lv"]="data_from_cisco_mqtt" \
["here_lv"]="data_from_here" \
["waze_tt_lv"]="data_from_wz_tt" \
["gr_data_original_lv"]="data_from_gr" \
["nexar_construction_original_lv"]="data_from_nexar_construction" \
["haas_original_lv"]="data_from_haas" \
["geotab_intersections_original_lv"]="data_from_geotab_intersections" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name='waycare_lv_database_original_bkp'

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="lv"

# Main bucket name:
bucket_name="waycare-data-original-bkp-encrypted"

# Backup bucket name:
backup_bucket_name="waycare-data-original-bkp"