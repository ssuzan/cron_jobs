#!/bin/bash

# Note carefully to the structure!
declare -A sources=( \
["waze_traffic_alerts_original_n_nv"]="data_from_wz" \
["waze_traffic_jam_original_n_nv"]="data_from_wz_jam" \
["climacell_rt_original"]="data_from_climacell_rt" \
["climacell_forecast_original"]="data_from_climacell_forecast" \
["tm_n_nv_original"]="data_from_tm" \
["noaa_original"]="data_from_noaa" \
["here_original"]="data_from_here" \
["ndex_ess_observation_original"]="data_from_ndex_ess_observation" \
["bsm_snow_plows_original"]="data_from_snow_plows" \
["construction_arcgis_original"]="data_from_n_nv_construction" \
["wz_road_closures_original"]="data_from_wz_road_closures" \
["geotab_nvdot_brooms_vehicles"]="data_from_geotab_nvdot" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name='waycare_n_nv_database_original_bkp'

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="n_nv"

# Main bucket name:
bucket_name="waycare-data-original-bkp-encrypted"

# Backup bucket name:
backup_bucket_name="waycare-data-original-bkp"