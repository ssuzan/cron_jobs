#!/bin/bash

# Note carefully to the structure!
declare -A sources=( \
["wz_delaware"]="data_from_wz" \
["wz_jam_delaware"]="data_from_wz_jam" \
["wz_irregularities_delaware"]="data_from_wz_irregularities" \
["wu_delaware"]="data_from_wu" \
["wu_forecast_delaware"]="data_from_wu_forecast" \
["travel_restrictions_delaware"]="data_from_travel_restrictions" \
["traveltimes_delaware"]="data_from_traveltimes" \
["vms_delaware"]="data_from_vms" \
["delaware_weather_districts_delaware"]="data_from_delaware_weather_districts" \
["delaware_weather_hydrology_levels_delaware"]="data_from_delaware_weather_hydrology_levels" \
["delaware_weather_snow_totals_delaware"]="data_from_delaware_weather_snow_totals" \
["delaware_weather_surface_delaware"]="data_from_delaware_weather_surface" \
["from_tmc_road_incidents_delaware"]="data_from_tmc_road_incidents" \
["tmc_road_closures_delaware"]="data_from_tmc_road_closures" \
["noaa_delaware"]="data_from_noaa" \
["tm_events_live_delaware"]="data_from_tm" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name='waycare_delaware_database_original_bkp'

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="delaware"

# Main bucket name:
bucket_name="waycare-data-original-bkp-encrypted"

# Backup bucket name:
backup_bucket_name="waycare-data-original-bkp"