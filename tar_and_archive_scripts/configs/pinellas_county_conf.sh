#!/bin/bash

# Note carefully to the structure!
declare -A sources=( \
["wz_pinellas_county"]="data_from_wz_pinellas" \
["wz_jam_pinellas_county"]="data_from_wz_jam" \
["bt_sensors_pinellas_county"]="data_from_bt_sensors" \
["pinellas_construction_pinellas_county"]="data_from_pinellas_construction" \
["twitter_pinellas_county"]="data_from_twitter" \
["noaa_pinellas_county"]="data_from_noaa" \
["seeclickfix_pinellas_county"]="data_from_seeclickfix" \
["tm_pinellas_county_original"]="data_from_tm" \
["911_events_pinellas_county_live"]="data_from_911_pinellas_county" \
["fhp_events_pinellas_county"]="data_from_fhp" \
["fl511_live_dms_pinellas_county"]="data_from_fl511_dms" \
["fl511_pinellas_county"]="data_from_fl511" \
["tampaelectric_pinellas_county_live"]="data_from_tampaelectric" \
["gr_data_original_pinellas_county"]="data_from_gr" \
["here_original"]="data_from_here" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name='waycare_pinellas_county_database_original_bkp'

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="pinellascounty"

# Main bucket name:
bucket_name="waycare-data-original-bkp-encrypted"

# Backup bucket name:
backup_bucket_name="waycare-data-original-bkp"
