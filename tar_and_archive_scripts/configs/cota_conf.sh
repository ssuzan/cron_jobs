#!/bin/bash

# Note carefully to the structure!
declare -A sources=( \
["noaa_original"]="data_from_noaa" \
["wz_original"]="data_from_wz" \
["wz_jam_original"]="data_from_wz_jam" \
["climacell_rt_original"]="data_from_climacell_rt" \
["climacell_forecast_original"]="data_from_climacell_forecast" \
["cota_alerts_original"]="data_from_cota_alerts" \
["cota_vehicles_original"]="data_from_cota_vehicles" \
["cota_trips_original"]="data_from_cota_trips" \
["cota_static_feed_original"]="data_from_cota_static_feed" \
["ohio_travel_delay_original"]="data_from_ohio_travel_delay" \
["ohio_dms_original"]="data_from_ohio_dms" \
["ohio_incidents_original"]="data_from_ohio_incidents" \
["ohio_construction_original"]="data_from_ohio_construction" \
["ohio_reported_conditions_original"]="data_from_ohio_reported_conditions" \
["twitter_original"]="data_from_twitter" \
["tm_original"]="data_from_tm" \
["here_original"]="data_from_here" \
["cota_stops_and_lines"]="data_from_cota_stops_and_lines" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name='waycare_cota_database_original_bkp'

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="cota"

# Main bucket name:
bucket_name="waycare-data-original-bkp-encrypted"

# Backup bucket name:
backup_bucket_name="waycare-data-original-bkp"