#!/bin/bash

# Note carefully to the structure!
declare -A sources=( \
["waze_traffic_alerts_original_tampa"]="data_from_wz_tampa" \
["weather_underground_original_live_tampa"]="data_from_wu" \
["wu_forecast_original_tampa"]="data_from_wu_forecast" \
["bt_sensors_original_live_tampa"]="data_from_bt_sensors" \
["tm_tampa_original"]="data_from_tm_tampa" \
["waze_traffic_jam_original_tampa"]="data_from_wz_jam_tampa" \
["waze_traffic_irregularities_original_tampa"]="data_from_wz_irregularities_tampa" \
["twitter_original_tampa"]="data_from_twitter_tampa" \
["8channel_tampa"]="data_from_8channel_tampa" \
["rc_original_tampa"]="data_from_rc_tampa" \
["intouch_gps_tampa"]="data_from_intouch_gps" \
["fl511_tampa"]="data_from_fl511" \
["noaa_tampa"]="data_from_noaa" \
["google_calendar_tampa"]="data_from_gc_tampa" \
["tampaelectric_tampa_live"]="data_from_tampaelectric" \
["gr_data_original_tampa"]="data_from_gr" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name='waycare_tampa_database_original_bkp'

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="tampa"

# Main bucket name:
bucket_name="waycare-data-original-bkp-encrypted"

# Backup bucket name:
backup_bucket_name="waycare-data-original-bkp"