#!/bin/bash

# Note carefully to the structure!
declare -A sources=( \
["henderson_news_original_henderson"]="data_from_henderson_news" \
["flood_control_original_henderson"]="data_from_flood_control" \
["tm_henderson_original"]="data_from_tm" \
["cad_henderson_original"]="data_from_cad_henderson" \
["acyclica_error_info_henderson_original"]="data_from_acyclica_error_info" \
["haas_henderson_original"]="data_from_haas" \
["henderson_construction_original"]="data_from_henderson_construction" \
["henderson_wz_travel_times"]="data_from_wz_tt" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name='waycare_henderson_database_original_bkp'

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="henderson"

# Main bucket name:
bucket_name="waycare-data-original-bkp-encrypted"

# Backup bucket name:
backup_bucket_name="waycare-data-original-bkp"