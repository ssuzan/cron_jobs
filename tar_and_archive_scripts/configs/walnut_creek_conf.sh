#!/bin/bash

# Note carefully to the structure!
declare -A sources=( \
["waze_traffic_alerts_original_walnutcreek"]="data_from_wz" \
["waze_traffic_jam_original_walnutcreek"]="data_from_wz_jam" \
["waze_traffic_irregularities_original_walnutcreek"]="data_from_wz_irregularities" \
["weather_underground_original_live_walnutcreek"]="data_from_wu" \
["wu_forecast_original_walnutcreek"]="data_from_wu_forecast" \
["bt_sensors_original_live_walnutcreek"]="data_from_bt_sensors" \
["twitter_original_walnut"]="data_from_twitter" \
["cad_original_walnutcreek"]="data_from_cad" \
["tm_original_walnutcreek"]="data_from_tm" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name='waycare_wc_database_original_bkp'

platform_name="walnutcreek"

# Main bucket name:
bucket_name="waycare-data-original-bkp-encrypted"

# Backup bucket name:
backup_bucket_name="waycare-data-original-bkp"