#!/bin/bash

# Note carefully to the structure!
declare -A sources=( \
["verizon_snow_plows_original"]="data_from_verizon_fleet_data" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name="waycare_n_nv_database_original_bkp"

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="n_nv"

# Main bucket name:
bucket_name="waycare-data-original-bkp-encrypted"

# Backup bucket name:
backup_bucket_name="waycare-data-original-bkp"