#!/bin/bash

# Note carefully to the structure!
declare -A sources=( \
["8_channel_original"]="data_from_8_channel" \
["fdot_construction_original"]="data_from_fdot_construction" \
["fhp_original"]="data_from_fhp" \
["gr_original"]="data_from_gr" \
["hcso_traffic_original"]="data_from_hcso_traffic" \
["hillsborough_construction_original"]="data_from_hillsborough_construction" \
["hillsborough_road_closure_original"]="data_from_hillsborough_road_closure" \
["noaa_original"]="data_from_noaa" \
["tampaelectric_original"]="data_from_tampaelectric" \
["tm_original"]="data_from_tm" \
["twitter_original"]="data_from_twitter" \
["wz_original"]="data_from_wz" \
["wz_irregularities_original"]="data_from_wz_irregularities" \
["wz_jam_original"]="data_from_wz_jam" \
["fl511_alerts_original"]="data_from_fl511" \
["climacell_rt_original"]="data_from_climacell_rt" \
["climacell_forecast_original"]="data_from_climacell_forecast" \
["here_original"]="data_from_here" \
["wz_original"]="data_from_wz" \
["wz_jam_original"]="data_from_wz_jam" \
)

# Set glacier name:
# NOTE: In case of a new platform, you'll need to create a new glacier vault at waycare support aws account
glacier_name='waycare_hillsborough_county_database_original_bkp'

# Platform name should be the same name as the folder name inside the "archive" folder which is inside the bucket "bucket_name"
platform_name="hillsborough_county"

# Main bucket name:
bucket_name="waycare-data-original-bkp-encrypted"

# Backup bucket name:
backup_bucket_name="waycare-data-original-bkp"