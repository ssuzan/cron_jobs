# Yaniv Costica
# yaniv.costica@waycaretech.com
# Cron file for tar and archive.
# Should be called with 1 input parameter: configuration file (example: "configs/lv_conf.sh")

import sys
import os
import apscheduler
from apscheduler.schedulers.blocking import BlockingScheduler

workspace_environ = str(os.environ['WORKSPACE'])
sys.path.append(workspace_environ+"/serverscripts_production/globals")
import universal_imports
sys.path.append(workspace_environ+"/serverscripts_production/globals/helpers")
import logs_handler


def tar_and_archive_main(conf_file):
    cmd = '/home/ubuntu/workspaces/cron_jobs/tar_and_archive_scripts/tar_and_archive_main.sh auto ' + conf_file + ' >> /home/ubuntu/workspaces/cron_jobs/cron_log.log &'
    print ('Executing: ' + cmd)
    os.system(cmd)
    print('tar_and_archive_main called with ' + conf_file + ' completed successfuly! time is: ' + universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S") + ' UTC')


def scheduled_call(conf_file):
    print('archive_scheduled_call started successfuly! time is: ' + universal_imports.datetime.utcnow().strftime("%d-%m-%Y-%H-%M-%S") + ' UTC')
    #initialize new scheduler
    sched = BlockingScheduler(timezone='UTC',coalesce=True)
    sched.add_job(tar_and_archive_main, 'cron', id='job_id1', hour='9', minute='5',coalesce=True, args=[conf_file])
    sched.start()


config_file = sys.argv[1]
scheduled_call(config_file)

