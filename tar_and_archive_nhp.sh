#setting date to run
if [ -z "$1" ]
  then
    echo "No argument supplied- please run: sh tar_and_archive_nhp.sh [dd_mm_yyyy or auto]"
    exit 1
fi

if [ $1 = "auto" ];
  then
    date="$(date -d "-1 days" +'%d_%m_%Y')"
else
    date="$1"
fi

# checking if already back up
file="$WORKSPACE/DB_backups/archive/.last_copied_nhp.txt"
if [ ! -f "$file" ]
  then
    echo "$0: File '${file}' not found."
  else
    while IFS= read -r line
    do
        # display $line or do somthing with $line
        if [ $line = $date ];
          then
            printf '%s has already been archived, exiting\n' "$line"
            exit 1
        fi
    done <"$file"
fi

echo "$0: starting backups for date" $date
echo $WORKSPACE/DB_backups/


#data_from_nhp
echo "working on NHP:"
find $WORKSPACE/DB_backups/data_from_nhp/database_original | grep $date  > /tmp/test.manifest
temp=$(cat /tmp/test.manifest | wc -l)
if [ $temp -gt 0 ]
  then
     if tar -cvzf /home/ubuntu/workspace/DB_backups/archive/data_from_nhp/road_incidents_nhp_original_live_lv_$date.tar.gz  --files-from /tmp/test.manifest
       then
         gpg2 --homedir /home/ubuntu/.gnupg --encrypt --recipient encrypt_nhp $WORKSPACE/DB_backups/archive/data_from_nhp/road_incidents_nhp_original_live_lv_$date.tar.gz
         rm $WORKSPACE/DB_backups/archive/data_from_nhp/road_incidents_nhp_original_live_lv_$date.tar.gz
           #archiving to S3:
         echo "archving to S3- NHP:"
         if s3cmd put $WORKSPACE/DB_backups/archive/data_from_nhp s3://waycare-data-original-bkp/archive/lv/ --recursive
          then
           placeholder=1
           #cleaning up:
           find $WORKSPACE/DB_backups/data_from_nhp/database_original | grep $date | xargs rm -v
           rm $WORKSPACE/DB_backups/archive/data_from_nhp/road_incidents_nhp_original_live_lv_$date.tar.gz.gpg
          else
          echo "failed S3 archive NHP"
        fi

        # #archiving to glacier:
        # echo "archving to clacier- NHP:"
        # if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_nhp/road_incidents_nhp_original_live_lv_$date.tar.gz.gpg -d "nhp_original_$date"
        #  then
        #   placeholder=1
        #  else
        #  echo "NHP Glacier archive failed"
        # fi
       else
         echo "NHP archive failed"
     fi
fi


##data_from_nhp
#tar -cvzf $WORKSPACE/DB_backups/archive/data_from_nhp/road_incidents_nhp_live_lv_$date.tar.gz $WORKSPACE/DB_backups/data_from_nhp/database_original/*_$date*.xml
#rm $WORKSPACE/DB_backups/data_from_nhp/database_original/*_$date*.xml
#
#data_from_nhp_loc
echo "working on NHP:"
find $WORKSPACE/DB_backups/data_from_nhp_loc/database_original | grep $date  > /tmp/test.manifest
temp=$(cat /tmp/test.manifest | wc -l)
if [ $temp -gt 0 ]
  then
     if tar -cvzf /home/ubuntu/workspace/DB_backups/archive/data_from_nhp_loc/road_incidents_nhp_loc_original_live_lv_$date.tar.gz  --files-from /tmp/test.manifest
       then
         gpg2 --homedir /home/ubuntu/.gnupg --encrypt --recipient encrypt_nhp $WORKSPACE/DB_backups/archive/data_from_nhp_loc/road_incidents_nhp_loc_original_live_lv_$date.tar.gz
         rm $WORKSPACE/DB_backups/archive/data_from_nhp_loc/road_incidents_nhp_loc_original_live_lv_$date.tar.gz
           #archiving to S3:
         echo "archving to S3- NHP LOC:"
         if s3cmd put $WORKSPACE/DB_backups/archive/data_from_nhp_loc s3://waycare-data-original-bkp/archive/lv/ --recursive
          then
           placeholder=1
           #cleaning up:
           find $WORKSPACE/DB_backups/data_from_nhp_loc/database_original | grep $date | xargs rm -v
           rm $WORKSPACE/DB_backups/archive/data_from_nhp_loc/road_incidents_nhp_loc_original_live_lv_$date.tar.gz.gpg
          else
          echo "S3 archive NHP LOC" 
        fi

        # #archiving to glacier:
        # echo "archving to clacier- NHP LOC: "
        # if python3 $WORKSPACE/cron_jobs/glacier_upload/main.py -v wayacre_lv_archive -f $WORKSPACE/DB_backups/archive/data_from_nhp_loc/road_incidents_nhp_loc_original_live_lv_$date.tar.gz.gpg -d "nhp_original_$date"
        #  then
        #   placeholder=1
        #  else
        #  echo "NHP LOC Glacier archive failed"
        # fi
       else
         echo "NHP LOC archive failed"
     fi
fi



##data_from_nhp_loc
#tar -cvzf $WORKSPACE/DB_backups/archive/data_from_nhp_loc/nhp_loc_live_lv_$date.tar.gz $WORKSPACE/DB_backups/data_from_nhp_loc/database_original/nhp_loc_live_lv_$date*.xml
#rm $WORKSPACE/DB_backups/data_from_nhp_loc/database_original/nhp_loc_live_lv_$date*.xml
#
#
#
##encrypting and archiving in s3:
#gpg2 --encrypt --recipient encrypt_nhp $WORKSPACE/DB_backups/archive/data_from_nhp/road_incidents_nhp_live_lv_$date.tar.gz
#gpg2 --encrypt --recipient encrypt_nhp $WORKSPACE/DB_backups/archive/data_from_nhp_loc/nhp_loc_live_lv_$date.tar.gz
#rm $WORKSPACE/DB_backups/archive/data_from_nhp/road_incidents_nhp_live_lv_$date.tar.gz
#rm $WORKSPACE/DB_backups/archive/data_from_nhp_loc/nhp_loc_live_lv_$date.tar.gz
#
#s3cmd put $WORKSPACE/DB_backups/archive s3://waycare-data-original-bkp/ --recursive
#rm $WORKSPACE/DB_backups/archive/data_from_nhp/road_incidents_nhp_live_lv_$date.tar.gz.gpg
#rm $WORKSPACE/DB_backups/archive/data_from_nhp_loc/nhp_loc_live_lv_$date.tar.gz.gpg
#
#
##writing latest backup date
echo $date > $WORKSPACE/DB_backups/archive/.last_copied_nhp.txt




